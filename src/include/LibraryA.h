#ifndef LIB_A_LIBRARYA_H
#define LIB_A_LIBRARYA_H

#pragma once

#ifdef _WIN32
#ifdef ggdepalib_EXPORTS
        #define GGDEPALIB_EXPORT __declspec(dllexport)
    #else
        #define GGDEPALIB_EXPORT __declspec(dllimport)
    #endif
#else
#define GGDEPALIB_EXPORT
#endif

namespace DependantALibrary {
    class GGDEPALIB_EXPORT LibraryA {

    public:
        static int Sum15(int a);

    };
}
#endif //LIB_A_LIBRARYA_H
