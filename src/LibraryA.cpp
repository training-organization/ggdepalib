
#include "include/LibraryA.h"
#include "MathLibrary.h"


namespace DependantALibrary {
    int LibraryA::Sum15(int a) {
        return MathLibrary::Functions::Add((double) a, (double) 15);
    }
}


