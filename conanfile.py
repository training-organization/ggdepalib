from conans import ConanFile, tools


class ALibConan(ConanFile):
    name = "ggdepalib"
    version = "0.1"
    license = "MIT"
    url = "https://bitbucket.org/ggarciatest/ggdepalib"
    description = "Dependant of libggcommon"
    settings = "os", "compiler", "build_type", "arch"
    requires = "libggcommon/0.1@demo/development"
    generators = "cmake"
    options = {"shared": [True, False]}
    default_options="shared=True"


    def package(self):
        self.copy("*.h", dst="include", src="src/include")
        self.copy("*.dll", dst="bin", src="build", keep_path=False)
        self.copy("*.so", dst="lib", src="build", keep_path=False)
        self.copy("*.lib", dst="lib", src="build", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)
